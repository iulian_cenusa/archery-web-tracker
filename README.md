# Archery Web Tracker #
## Author: Iulian Cenusa ##
## September 2020 ##
# #

This web application is designed to keep track of your archery accuracy.
For this you have to introduce the ammount of arrows fired in a single session and how many of them hit a certain circle of the bullseye.

## Setup ##

In order to setup correctly please run:
- python3 manage.py makemigrations
- python3 manage.py migrate

## Features ##

+ Home page that displays all entries
+ Add and edit form
+ Delete unwanted entries
+ You can't introduce more or less arrows hit than arrows submitted
