from django.urls import path
from .views import Home , DeleteArchery , UpdateArchery , AddFormF
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    #---Home---
    path('', Home.as_view(), name = 'Home' ),
    #---Model Operations---
    #path('add/', AddArchery.as_view(), name = 'AddA'),
    path('<int:pk>/edit',UpdateArchery.as_view(),name = 'EditA'),
    path('<int:pk>/delete',DeleteArchery.as_view(),name = 'DeleteA'),
    #---Functions---
    path('add/', AddFormF, name = 'AddAF'),
]

urlpatterns += staticfiles_urlpatterns()