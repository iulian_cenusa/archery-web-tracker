from django.shortcuts import render, HttpResponse, redirect
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import Archery
from django.urls import reverse_lazy
from .forms import AddForm, EditForm
from django.contrib import messages

#def home_view(request):
#    return render(request,'home.html',{})

#---Home---
class Home(ListView):
    model = Archery
    template_name = 'home.html'

#---Model operations---
#class AddArchery(CreateView):
#    model = Archery
#    form_class = AddForm
#    template_name = 'add_archery.html'

class UpdateArchery(UpdateView):
    model = Archery
    form_class = EditForm
    template_name = 'edit_archery.html'

class DeleteArchery(DeleteView):
    model = Archery
    template_name = 'delete_archery.html'
    success_url = reverse_lazy('Home')

#---Function Form---
def AddFormF(response):
    form = AddForm()
    if response.method == 'POST':
        form = AddForm(response.POST)
        #print ( response.POST )

        if ( int(response.POST.get('p_25')) + int(response.POST.get('p_20')) + int(response.POST.get('p_17')) + int(response.POST.get('p_15')) + int(response.POST.get('p_10')) + 
        int(response.POST.get('p_7')) + int(response.POST.get('p_5')) + int(response.POST.get('p_4')) + int(response.POST.get('p_3')) + int(response.POST.get('p_2')) +
        int(response.POST.get('p_1')) + int(response.POST.get('p_0')) < int(response.POST.get('arrows'))):
            messages.add_message(response, messages.ERROR, 'Too few arrows')
            return redirect('AddAF')

        if ( int(response.POST.get('p_25')) + int(response.POST.get('p_20')) + int(response.POST.get('p_17')) + int(response.POST.get('p_15')) + int(response.POST.get('p_10')) + 
        int(response.POST.get('p_7')) + int(response.POST.get('p_5')) + int(response.POST.get('p_4')) + int(response.POST.get('p_3')) + int(response.POST.get('p_2')) +
        int(response.POST.get('p_1')) + int(response.POST.get('p_0')) > int(response.POST.get('arrows'))):
            messages.add_message(response, messages.ERROR, 'Too many arrows')
            return redirect('AddAF')

        if form.is_valid():
            form.save()
            messages.add_message(response, messages.INFO, 'Entry successfully added')
            return redirect('AddAF')
        else:
            form = AddForm()
    return render(response,'add_archery.html',{'form':form})