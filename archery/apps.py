from django.apps import AppConfig


class ArcheryConfig(AppConfig):
    name = 'archery'
