from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.core.exceptions import ValidationError

class Archery(models.Model):
    creation_date = models.DateTimeField(default=timezone.now,editable=False)
    name = models.CharField(max_length=20,default='')
    arrows = models.IntegerField(default=1)
    points = models.IntegerField(default=0)
    acc = models.IntegerField(default=0)
    p_25 = models.IntegerField(default=0)
    p_20 = models.IntegerField(default=0)
    p_17 = models.IntegerField(default=0)
    p_15 = models.IntegerField(default=0)
    p_10 = models.IntegerField(default=0)
    p_7 = models.IntegerField(default=0)
    p_5 = models.IntegerField(default=0)
    p_4 = models.IntegerField(default=0)
    p_3 = models.IntegerField(default=0)
    p_2 = models.IntegerField(default=0)
    p_1 = models.IntegerField(default=0)
    p_0 = models.IntegerField(default=0)

    def save(self, *args, **kwargs): 
        lst = (self.p_0, self.p_1,self.p_2, self.p_3, self.p_4, self.p_5, self.p_7, self.p_10, self.p_15, self.p_17, self.p_20, self.p_25 )
        if sum(lst) == self.arrows:
            self.points = self.p_0*0 + self.p_1*1 + self.p_2*2 + self.p_3*3 + self.p_4*4 + self.p_5*5 + self.p_7*7 + self.p_10*10 + self.p_15*15 + self.p_17*17 + self.p_20*20 + self.p_25*25
            self.acc = self.points / ( self.arrows * 25 ) * 100
            super(Archery, self).save(*args, **kwargs) 
    
    def get_absolute_url(self):
        return reverse('Home')
