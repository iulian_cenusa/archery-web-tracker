from django import forms
from .models import Archery

class AddForm ( forms.ModelForm):

    class Meta:
        model = Archery
        fields = ['name','arrows','p_25','p_20','p_17','p_15','p_10','p_7','p_5','p_4','p_3','p_2','p_1','p_0',]

        labels = {
            'name': ('Writer'),
        }
        help_texts = {
            'name': ('Some useful help text.'),
        }
        error_messages = {
            'name': {
                'max_length': ("This writer's name is too long."),
            },
        }

class EditForm ( forms.ModelForm):

    class Meta:
        model = Archery
        fields = ['name','arrows','p_25','p_20','p_17','p_15','p_10','p_7','p_5','p_4','p_3','p_2','p_1','p_0',]

        widgets ={
            'name': forms.TextInput(attrs={ 'class': 'form-control' }),
            'arrows': forms.NumberInput(attrs={ 'class': 'form-control' }),
            'p_25': forms.NumberInput(attrs={ 'class': 'form-control' }),
            'p_20': forms.NumberInput(attrs={ 'class': 'form-control' }),
            'p_17': forms.NumberInput(attrs={ 'class': 'form-control' }),
            'p_15': forms.NumberInput(attrs={ 'class': 'form-control' }),
            'p_10': forms.NumberInput(attrs={ 'class': 'form-control' }),
            'p_7': forms.NumberInput(attrs={ 'class': 'form-control' }),
            'p_5': forms.NumberInput(attrs={ 'class': 'form-control' }),
            'p_4': forms.NumberInput(attrs={ 'class': 'form-control' }),
            'p_3': forms.NumberInput(attrs={ 'class': 'form-control' }),
            'p_2': forms.NumberInput(attrs={ 'class': 'form-control' }),
            'p_1': forms.NumberInput(attrs={ 'class': 'form-control' }),
            'p_0': forms.NumberInput(attrs={ 'class': 'form-control' }),
        }